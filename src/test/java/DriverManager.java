import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.util.concurrent.TimeUnit;


public class DriverManager {


    public static WebDriver getDriver(String browser) {
        if (OsValidator.OSName().equals("win") || OsValidator.OSName().equals("unix")) {
            browser = browser.concat("-".concat(OsValidator.OSName()));
        }
        System.out.println(browser);
        WebDriver b = null;
        switch (browser) {
            case "firefox-win":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(DriverManager.class.getResource("/geckodriver.exe").getFile()).getPath());
                b = new FirefoxDriver();
                break;
            case "firefox-unix":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(DriverManager.class.getResource("/geckodriver").getFile()).getPath());
                b = new FirefoxDriver();
                break;
            case "chrome-win":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver.exe").getFile()).getPath());
                b = new ChromeDriver();
                break;
            case "chrome-unix":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver").getFile()).getPath());
                b = new ChromeDriver();
                break;
            case "ie-win":
            case "internet explorer-win":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/IEDriverServer.exe").getFile()).getPath());
                b = new ChromeDriver();
                break;
            case "ie-unix":
            case "internet explorer-unix":
                System.out.println("The Internet Explorer browser is not supported in this operation system");
                break;
            default:
                b = new ChromeDriver();
        }
        return b;
    }

    public static EventFiringWebDriver getConfigureDriver(String browser) {
        EventFiringWebDriver driver = new EventFiringWebDriver(getDriver(browser));
        driver.register(new EventHandler());

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        return driver;
    }

}
                                           