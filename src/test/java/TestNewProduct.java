import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.util.List;
import java.util.Random;


public class TestNewProduct {

    public static final String URL1 = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";
    public static final String URL2 = "http://prestashop-automation.qatestlab.com.ua/";
    public WebDriver driver;
    public WebDriverWait wait;
    Random random = new Random();
    String productName = productNameGenerator();
    int productQuantity = random.nextInt(100) + 1;
    double productPrice = (random.nextInt(10000) + 1) / 100;


    @BeforeTest
    @Parameters("param")
    public void setUp(String param) {
        log("Preparing the WebDriver " + param);
        try {
            driver = DriverManager.getDriver(param);
            driver.manage().window().maximize();
            wait = new WebDriverWait(driver, 15);
        } catch (NullPointerException e) {
            log("The WebDriver is not launched");
            e.printStackTrace();
        }
    }

    @AfterTest
    public void tearDown() {
        log("The test is finished");
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void prepareEnvironment() {
        System.out.println("Next test is starting");
    }

    @AfterMethod
    public void clearEnvironment() {
        System.out.println("The test has finished");
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "loadUserFromFile")

    public void testNewProduct(String userName, String userPassword) {
        Assert.assertTrue(driver != null, "The WebDriver is not launched");
        try {
            log("Navigate to admin panel: " + URL1);
            driver.get(URL1);
            log("Log in with username: " + userName);
            logIn(userName, userPassword);
            log("Navigate to product page in admin-panel");
            moveToProductsAdminPage();
            log("Create new product");
            createNewProduct(productName, productQuantity, productPrice);
        } catch (NullPointerException e) {
            log("The test is skipped with Exception " + e);
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = "testNewProduct")
    public void checkNewProduct() {
        System.out.println("This is my Test Case 2");
        Assert.assertTrue(driver != null, "The WebDriver is not launched");
        try {
            log("Navigate to Home-page of the site: " + URL2);
            driver.navigate().to(URL2);
            log("Search the product with entered data");
            searchProductOnPage();
            log("Check attributtes of the found product");
            checkingProductAttributtes();
        } catch (NullPointerException e) {
            log("The test is skipped with exception: " + e);
            e.printStackTrace();
        }
    }


    public void logIn(String userLogin, String userPassword) {
        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys(userLogin);
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys(userPassword);
        WebElement submitButton = driver.findElement(By.name("submitLogin"));
        submitButton.click();

    }

    public void moveToProductsAdminPage() {
//        Move to the catalog in the main menu
        waitingById("subtab-AdminCatalog");
        WebElement menuitem = driver.findElement(By.id("subtab-AdminCatalog"));
        Actions builder = new Actions(driver);
        builder.moveToElement(menuitem).build().perform();
//        Waiting for the catalog-link appearing
        waitingById("subtab-AdminProducts");
        driver.findElement(By.id("subtab-AdminProducts")).click();
    }

    private String productNameGenerator() {
        String[] productColor = {"Red", "Green", "Blue", "Yellow", "White", "Black",
                "Brown", "Grey", "Pink", "Purple", "Printed", "Colorful", ""};
        String[] productType = {"Dress", "Blouse", "Skirt", "Shirt", "T-shirt", "Pants",
                "Shorts", "Cardigan", "Sweater", "Suit", "Vest"};
        String s = productColor[random.nextInt(productColor.length)]
                .concat(" " + productType[random.nextInt(productType.length)]);
        return s;
    }

    private void createNewProduct(String productName, int productQuantity, double productPrice) {
        waitingById("page-header-desc-configuration-add");
//        Move to the new category page
        driver.findElement(By.id("page-header-desc-configuration-add")).click();
        waitingById("form_step1_price_shortcut");
        log("Enter the product name: " + productName);
        driver.findElement(By.id("form_step1_name_1")).sendKeys(productName);
        log("Enter the quantity of the product: " + productQuantity);
        driver.findElement(By.id("form_step1_qty_0_shortcut")).sendKeys(Integer.toString(productQuantity));
        log("Enter the product price: " + productPrice);
        WebElement price = driver.findElement(By.id("form_step1_price_shortcut"));
        price.clear();
        price.sendKeys(Double.toString(productPrice));
        log("Activate the product on the site");
        driver.findElement(By.className("switch-input")).click();
        growlMessage();
        log("Save the product");
        driver.findElement(By.cssSelector("button.btn.btn-primary.js-btn-save")).click();
        growlMessage();
    }

    private void growlMessage() {
        try {
            waitingByClassName("growl-message");
            WebElement message = driver.findElement(By.className("growl-message"));
            log("The message '" + message.getText() + "' is displayed");
            driver.findElement(By.className("growl-close")).click();
            wait.until(ExpectedConditions.invisibilityOf(message));
        } catch (NoSuchElementException e) {
            log("The information message is not displayed");
        }
    }

    private void searchProductOnPage() {

        waitingByClassName("all-product-link");
        driver.findElement(By.className("all-product-link")).click();
        waitingById("js-product-list");
        List<WebElement> products = driver.findElements(By.cssSelector(".h3.product-title > a"));
        int productIndex = -1;
        String s = null;
        for (int i = 0; i < products.size(); i++) {
            s = products.get(i).getText();
            if (productName.equals(s)) {
                productIndex = i;
            }
        }
        Assert.assertTrue(productIndex!=-1,"The product '" + productName + "' is not found");
        if (productIndex == -1) {
            log("The product '" + productName + "' is not found");
        } else {
            products.get(productIndex).click();
        }
    }

    private void checkingProductAttributtes() {
        waitingByClassName("footer-container");
//        Reading the displayed product attributes
        String realProductName = driver.findElement(By.className("h1")).getText().toLowerCase();
        String realPrice = driver.findElement(By.className("current-price")).getText();
        realPrice = realPrice.substring(0, realPrice.indexOf(" ")).replace(",", ".");
        String realQuantity = driver.findElement(By.cssSelector(".product-quantities>span")).getText();
        realQuantity = realQuantity.substring(0, realQuantity.indexOf(" "));
        log("Read actual product attributtes");
        log("Te actual product name: " + realProductName);
        log("Te actual product price: " + realPrice);
        log("Te actual quantity: " + realQuantity);
        double realPriceToDouble = Double.valueOf(realPrice);
        int realQuantityToInt = Integer.valueOf(realQuantity);
//        Execute product attribute verifications
        Assert.assertEquals(realPriceToDouble, productPrice, "The displayed price '" + realPrice
                + "' is not equal to the entered price '" + productPrice + "'.");
        Assert.assertEquals(realProductName, productName.toLowerCase(),
                "The product name is not mismatch");
        Assert.assertEquals(realQuantityToInt, productQuantity, "The displayed quantity '" + realQuantity
                + "' is not equal to the entered quantity '" + productQuantity + "'.");
    }

    public void waitingByClassName(String className) {
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.className(className)));
    }

    public void waitingById(String id) {
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    }

    private void log(String message) {
        Reporter.log(message + "<br>");
    }


}
